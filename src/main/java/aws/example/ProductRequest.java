package aws.example;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRequest {
    private String id;
    private String title;
    private String pictureUrl;
    private double price;
}

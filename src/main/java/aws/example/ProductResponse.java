package aws.example;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import lombok.Builder;
import lombok.Data;
import java.util.List;
import java.util.Map;

@Data
@Builder
public class ProductResponse {
    private String message;
    private List<Map<String, AttributeValue>> products;
}

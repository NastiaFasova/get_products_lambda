package aws.example;

public class UnableToSaveItemException extends RuntimeException {
    public UnableToSaveItemException(String message, Throwable cause) {
        super(message, cause);
    }
}
